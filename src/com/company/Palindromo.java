package com.company;

/**
 * Created by july on 05/07/2017.
 */
public class Palindromo {
    public boolean espalindromo(String cadena){
        boolean valor=true;
        int i,ind;
        String cadena2="";
        //quitar los espacios
        for (int x=0; x < cadena.length(); x++) {
            if (cadena.charAt(x) != ' ')
                cadena2 += cadena.charAt(x);
        }
        //volver a asignar variables
        cadena=cadena2;
        ind=cadena.length();
        //comparar cadenas
        for (i= 0 ;i < (cadena.length()); i++){
            if (cadena.substring(i, i+1).equals(cadena.substring(ind - 1, ind)) == false ) {
                //si una sola letra no corresponde no es un palindromo por tanto
                //sale del ciclo con valor false
                valor=false;
                break;
            }
            ind--;
        }
        return valor;
    }
}
